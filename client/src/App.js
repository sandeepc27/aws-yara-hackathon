import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import Login from './container/Login';
import Admin from './container/Admin';
import Employee from './container/Employee';
import AddHackathon from './container/AddHackathon';
import AddTeam from './container/AddTeam';
import EditTeam from './container/EditTeam';
import AlreadyVoted from './container/AlreadyVoted';
import RegDetails from './container/RegDetails';
import EditHackathon from './container/EditHackathon';
import EmployeeVote from './container/EmployeeVote';
import NoTeamRegistered from './container/NoTeamRegistered';
import NoTeamVote from './container/NoTeamVote';
import NoTeamToVote from './container/NoTeamToVote';
import EmployeeWinner from './container/EmployeeWinner';
import WinnerEmployee from './container/WinnerEmployee';
import WinnerAdmin from './container/WinnerAdmin';
import {QueryClientProvider, QueryClient} from 'react-query';
import { ChakraProvider } from '@chakra-ui/react'


const queryClient = new QueryClient()

function App() {
  return (
    <ChakraProvider>
    <QueryClientProvider client={queryClient}>
    <Router>
    <div style={{marginLeft: '10px'}}>
       <Routes>
         <Route exact path="/" element={<Login/>}/>
         <Route exact path="/Admin" element={<Admin/>}/>
         <Route exact path="/Employee" element={<Employee/>}/>
         <Route exact path="/Login" element={<Login/>}/>
         <Route exact path="/AlreadyVoted" element={<AlreadyVoted/>}/>
         <Route exact path="/NoTeamToVote" element={<NoTeamToVote/>}/>
         <Route exact path="/AddTeam" element={<AddTeam/>}/>
         <Route exact path="/EditTeam" element={<EditTeam/>}/>
         <Route exact path="/AddHackathon" element={<AddHackathon/>}/>
         <Route exact path="/EditHackathon" element={<EditHackathon/>}/>
         <Route exact path="/HackathonDetail" element={<RegDetails/>}/>
         <Route exact path="/Vote" element={<EmployeeVote/>}/>
         <Route exact path="/NoTeamRegistered" element={<NoTeamRegistered/>}/>
         <Route exact path="/NoTeamVote" element={<NoTeamVote/>}/>
         <Route exact path="/EmployeeWinner" element={<EmployeeWinner/>}/>
         <Route exact path="/ExpireHackathonEmployee" element={<WinnerEmployee/>}/>
         <Route exact path="/WinnerAdmin" element={<WinnerAdmin/>}/>
       </Routes>
    </div>
   </Router>
   </QueryClientProvider>
   </ChakraProvider>
  );
}

export default App;
