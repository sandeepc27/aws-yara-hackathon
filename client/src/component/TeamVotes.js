import "../styles/RegDetailStyles.css";
import { useNavigate } from "react-router-dom";
import React,{Fragment,useState,useEffect} from 'react';
import Hero from "../component/Hero";
import { useParams } from 'react-router';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import {getVotesCount} from "../services/apiVote";
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
    Button,
    Text
  } from '@chakra-ui/react'

const TeamVotes=(props)=>{
    const navigate = useNavigate();
    const [details,setDetails] = useState([]);
    //const hackathonid = useParams();
    const hackathonid = props.hackathonsid;

    const loadDetails = async ()=>{
        setDetails(await getVotesCount(hackathonid));
    }

    useEffect(()=>{
        loadDetails();
      },[]);
    
    //To check whether data is present
    const docheck=()=>
    {
        if(details.length==null)
            navigate('/NoTeamVote');
    }

return(
    <div>
        <div>
        <Table className="tbl1">
           <Thead> 
                <Tr>
                    <Th>Team Id</Th>
                    <Th>Team Name</Th>
                    <Th>Problem Statement</Th>
                    <Th>Member Id</Th>
                    <Th>Member Name</Th>
                    <Th>Vote</Th>
                </Tr>
            </Thead>
             <Tbody>
             {docheck()}
                {details.map((item) => {
                    return(
                        <Tr key={item.teamid}>
                            <Td>{item.teamid}</Td>
                            <Td>{item.teamname}</Td>
                            <Td>{item.problem}</Td>
                            <Td>
                                <Table>
                                    {item.members.map(member => (
                                    <Tr key={member.id}>
                                        <Td><center>{member.id}</center></Td>
                                    </Tr>
                                    ))}
                                </Table>
                            </Td>
                            <Td>
                                <Table>
                                    <Tr></Tr>
                                    {item.members.map(member => (
                                    <Tr key={member.id}>
                                        <Td><center>{member.name}</center></Td>
                                    </Tr>
                                    ))}
                                </Table>
                            </Td>
                            <td>{item.count}</td>
                        </Tr>
                    )
                }
                )}
            </Tbody> 
        </Table>        
        </div>
    </div>
    )
}
export default TeamVotes

