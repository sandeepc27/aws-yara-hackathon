import React,{Fragment,useState,useEffect} from 'react';
import {
    Box,
    Center,
    useColorModeValue,
    Heading,
    Button,
    Text,
    Stack,
    Image,
  } from '@chakra-ui/react';

import { useNavigate ,useLocation} from "react-router-dom";
import { getOngoingHackathon } from '../services/apiHackathon';

const Ongoing=()=>{
  const navigate = useNavigate();
  const [detail,setDetail] = useState([])
  const location = useLocation();
  var employeeid = 'a123001';
   
  //To edit hackathon details
  const doEdit = (hackathonsid) => {
    navigate('/EditHackathon',{state:{hackathonsid,employeeid}});
  }

  //To change date format
  const changeDateFormat = (d) => {
    let date = new Date(d);
    let month = date.getMonth()+1;
    let result = date.getDate() + '-' + month + '-' + date.getFullYear();
    return result;
  }

  const loadHackathons = async ()=>{
    setDetail(await getOngoingHackathon());
  }

  useEffect(()=>{
    loadHackathons();
  },[]);
    
  //To see all registered team
  const doFetch = (hackathonid) => {
    var hackathonsid= hackathonid
    navigate('/HackathonDetail',{state:{hackathonsid,employeeid}});
  }

  //to check if any hackathon is registered
  if(detail.length==0){
    return( <h3>No Ongoing Hackathon</h3>)
  }

  return( 
    <Fragment>
      <section className="expires">
        <Center>
          <Heading fontSize={'5xl'} fontFamily={'body'} fontWeight={400}>
            Ongoing Hackathons
          </Heading>
        </Center>
        <div className="row">
          {
            detail.map((detail)=>{
              return(
                <div className="column">
                  <div className="single-expire">
                    <Center py={12}>
                      <Box role={'group'} p={6} maxW={'330px'} w={'full'} boxShadow={'2xl'} rounded={'lg'} pos={'relative'} zIndex={1}>
                        <Box
                          rounded={'lg'}
                          mt={-12}
                          pos={'relative'}
                          height={'230px'}
                          _after={{
                            transition: 'all .3s ease',
                            content: '""',
                            w: 'full',
                            h: 'full',
                            pos: 'absolute',
                            top: 5,
                            left: 0,
                            backgroundImage: `url(${detail.poster})`,
                            filter: 'blur(15px)',
                            zIndex: -1,
                          }}
                          _groupHover={{
                            _after: {
                              filter: 'blur(20px)',
                            },
                        }}>
                          <Image rounded={'lg'} height={230} width={282} objectFit={'cover'} src={detail.poster} />
                        </Box>
                        <Stack pt={10} align={'center'}>
                          <Text color={'gray.500'} fontSize={'sm'} textTransform={'uppercase'}>
                            Hackathon
                          </Text>
                          <Heading fontSize={'2xl'} fontFamily={'body'} fontWeight={500}>
                            {detail.hackathonname}
                          </Heading>
                          <Stack direction={'row'} align={'center'}>
                            <Image height={5} width={22} objectFit={'cover'} src={'https://drive.google.com/uc?export=view&id=1kMbZOwJsDJrd3B8COrnO0kChyMJJ2DqS'} />
                            <Text fontWeight={700} fontSize={'l'} pr={5}>
                              {changeDateFormat(detail.hackathondate)}
                            </Text>
                            <Image height={5} width={22} objectFit={'cover'} src={'https://drive.google.com/uc?export=view&id=11PLH_szHogJL6z5Pi9CahqkCka40ojTs'}/>
                            <Text fontWeight={700} fontSize={'l'}>
                              {detail.prize}
                            </Text>
                          </Stack>
                          <Stack direction={'row'}>
                            <Button bg={'blue.400'} rounded={'full'} color={'white'} onClick={()=>doFetch(detail.hackathonid)}  _hover={{ bg: 'blue.600' }}> View Teams </Button>
                            <Text pl={5}></Text>
                            <Button bg={'blue.400'} rounded={'full'} color={'white'} onClick={()=>doEdit(detail.hackathonid)}  _hover={{ bg: 'blue.600' }}> Update </Button>
                          </Stack>
                        </Stack>
                      </Box>
                    </Center>
                  </div>
                </div>
                )
              })  
            }  
          </div>
      </section>
    </Fragment>
  )
 }
 export default Ongoing;



