import React,{useState} from "react";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Expire from "./Expire";
import EmployeeOngoing from "./EmployeeOngoing";
import EmployeeExpire from "./EmployeeExpire";
import "../styles/TabsStyles.css";

export default () => (
 
<Tabs>
  <div className="tab">
    <TabList>
      <Tab><span>Ongoing Hackathon</span></Tab>
      <Tab><span>Expired Hackathon</span></Tab>
    </TabList>
    </div>
    <TabPanel>
      <EmployeeOngoing/>
    </TabPanel>
    <TabPanel>
      <EmployeeExpire/>
    </TabPanel>
  </Tabs>
 );