import "../styles/RegDetailStyles.css";
import { useNavigate } from "react-router-dom";
import React,{Fragment,useState,useEffect} from 'react';
import { useParams } from 'react-router';
import {getVotesCount} from "../services/apiVote";
import {updateWinners} from "../services/apiHackathon";
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
    Button,
    Text,
    Center
  } from '@chakra-ui/react'

const WinningTeam=(props)=>{
    const navigate = useNavigate();
    const [details,setDetails] = useState([]);
    const [winningdetails,setWinningDetails] = useState({winner:'',runner:'',hackathonid:''});
    //const hackathonid = useParams();
    const hackathonid = props.hackathonsid;

    const loadDetails = async ()=>{
        setDetails(await getVotesCount(hackathonid));
    }

    useEffect(()=>{
        loadDetails();
    },[]);
    
    //To check whether data is present
    const docheck=()=>
    {
        if(details.length==null)
            navigate('/NoTeamVote');
    }

    const updateWinner=async()=>
    { 
        await updateWinners({winner: details[0].teamname,runner:details[1].teamname,hackathonid: hackathonid});
    }
    
    return(
        <div>
            <div>  
            {docheck()}    
                {details.map(function(item,i){
                    if(i<3){
                        if(i%2==0){
                        return(
                            <div>
                                <div className="first-des">
                                    <div className="des-text">
                                        <Center> <Text fontWeight={700} fontSize={25} >{item.teamid}</Text></Center><br/>
                                        <Center> <Text fontWeight={700} fontSize={25} >{item.teamname}</Text></Center><br/>
                                        <Center> <Text fontWeight={400} fontSize={20} >Problem statement :</Text></Center>
                                        <Center> <Text fontWeight={400} fontSize={20} >{item.problem}</Text></Center><br/>
                                        <div>
                                            <table className="tbl1">
                                                <tr>
                                                    <td>Employee ID</td>
                                                    <td>Employee Name</td>
                                                </tr>
                                                {item.members.map(member => (
                                                    <tr key={member.id}>
                                                        <td><center>{member.id}</center></td>
                                                        <td><center>{member.name}</center></td>
                                                    </tr>
                                                ))}
                                            </table>
                                        </div><br/><br/>
                                        <Center> <Text fontWeight={400} fontSize={25} > Vote Count: {item.count}</Text></Center><br/>
                                    </div>
                                    <div className="images1">
                                        <img alt="img" src="https://drive.google.com/uc?export=view&id=17cH_LBpayJWmYbWx5lNibTK2uyQJ9RLX"/>
                                        <img alt="img" src="https://drive.google.com/uc?export=view&id=1DPBKmvtEfS_OUVZW6OmyUQ8JUFV8dOiW"/>
                                    </div>
                                </div>
                            <br/>
                            <hr/>
                        </div>
                        )
                    }
                    else{
                        return(
                            <div className="first-des">
                                <div className="images1">
                                    <img alt="img" src="https://drive.google.com/uc?export=view&id=1aTAUKBWD9kIM4X9OEMyjaaRPjPfpCnJg"/>
                                    <img alt="img" src="https://drive.google.com/uc?export=view&id=19Rk7YSvmaCpeFlEsFAKnumads_c4EKp0"/>
                                </div>
                                <div className="des-text">
                                   <Center> <Text fontWeight={700} fontSize={25} >{item.teamid}</Text></Center><br/>
                                   <Center> <Text fontWeight={700} fontSize={25} >{item.teamname}</Text></Center><br/>
                                   <Center> <Text fontWeight={400} fontSize={20} >Problem statement :</Text></Center>
                                   <Center> <Text fontWeight={400} fontSize={20} >{item.problem}</Text></Center><br/>
                                    <div>
                                        <table className="tbl1">
                                            <tr>
                                                <td>Employee ID</td>
                                                <td>Employee Name</td>
                                            </tr>
                                            {item.members.map(member => (
                                                <tr key={member.id}>
                                                    <td><center>{member.id}</center></td>
                                                    <td><center>{member.name}</center></td>
                                                </tr>
                                            ))}
                                        </table>
                                    </div><br/><br/>
                                <Center> <Text fontWeight={400} fontSize={25} > Vote Count: {item.count}</Text></Center><br/>
                            </div>
                        </div>
                    )
                }
            }
        })}
        <br/>
        <hr/>
        <div className="UpdateButton"><Button onClick={() => updateWinner()}>Update Winners</Button></div>
    </div> 
</div>
)
}
export default WinningTeam

