import React,{Fragment,useState,useEffect} from 'react';
import '../styles/ExpireStyles.css'
import { useNavigate, useLocation } from "react-router-dom";
import { getExpiredHackathon } from '../services/apiHackathon';
import { Box, Center, Heading, Button, Text, Stack, Image } from '@chakra-ui/react';

const Expire=()=>{
    const navigate = useNavigate();
    const [detail,setDetail] = useState([])
    const location = useLocation();
    const employeeid = location.state.employeeid;

    const loadHackathons = async ()=>{
      setDetail(await getExpiredHackathon());
    }
    
    useEffect(()=>{
      loadHackathons();
    },[]);

    const changeDateFormat = (d) => {
      let date = new Date(d);
      let result = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();
      return result;
    }

    //to check any hackathon is registered
    if(detail.length== 0){
      <h1>No Expired hackathons Yet</h1>
    }
    const ExpireDetails=(hackathonid)=>{
      navigate('/ExpireHackathonEmployee',{state:{employeeid,hackathonid}});
    }

    return( 
      <Fragment>
        <section className="expire">
          <div className="center">
            <h3>Hackathons</h3>
            </div>
              <div  className="row">
                {
                  detail.map((detail)=>{
                    return(
                        <div className="column">
                        <div className="single-expire">
                          <Center py={12}>
                            <Box role={'group'} p={6} maxW={'330px'} w={'full'} boxShadow={'2xl'} rounded={'lg'} pos={'relative'} zIndex={1}>
                              <Box
                                rounded={'lg'}
                                mt={-12}
                                pos={'relative'}
                                height={'230px'}
                                _after={{
                                  transition: 'all .3s ease',
                                  content: '""',
                                  w: 'full',
                                  h: 'full',
                                  pos: 'absolute',
                                  top: 5,
                                  left: 0,
                                  backgroundImage: `url(${detail.poster})`,
                                  filter: 'blur(15px)',
                                  zIndex: -1,
                                }}
                                _groupHover={{
                                  _after: {
                                    filter: 'blur(20px)',
                                  },
                                }}>
                                <Image rounded={'lg'} height={230} width={282} objectFit={'cover'} src={detail.poster}/>
                              </Box>
                              <Stack pt={10} align={'center'}>
                                <Text color={'gray.500'} fontSize={'sm'} textTransform={'uppercase'}>
                                  Hackathon
                                </Text>
                                <Heading fontSize={'2xl'} fontFamily={'body'} fontWeight={500}>
                                  {detail.hackathonname}
                                </Heading>
                                <Stack direction={'row'} align={'center'}>
                                <Image height={5} width={22} objectFit={'cover'} src={'https://drive.google.com/uc?export=view&id=1kMbZOwJsDJrd3B8COrnO0kChyMJJ2DqS'}/>
                                  <Text fontWeight={700} fontSize={'l'} pr={5}>
                                  {changeDateFormat(detail.hackathondate)}
                                  </Text>
                                  <Image height={5} width={22} objectFit={'cover'} src={'https://drive.google.com/uc?export=view&id=11PLH_szHogJL6z5Pi9CahqkCka40ojTs'}/>
                                  <Text fontWeight={700} fontSize={'l'}>
                                    {detail.prize}
                                  </Text>
                                </Stack>
                                <Stack direction={'row'}>
                                  <Button bg={'blue.400'} rounded={'full'} color={'white'} onClick={()=>ExpireDetails(detail.hackathonid)} _hover={{ bg: 'blue.600' }}>
                                    View Details
                                  </Button>
                                  <Text pl={5}></Text>
                                </Stack>
                              </Stack>
                            </Box>
                          </Center>
                        </div> 
                    </div>
                    )
                  })  
                }  
                </div>
            </section>
        </Fragment>
    )
 }
 export default Expire;
