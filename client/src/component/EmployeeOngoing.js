import React,{Fragment,useState,useEffect} from 'react';
import '../styles/OngoingStyles.css'
import { useNavigate,useLocation } from "react-router-dom";
import { getOngoingHackathon } from '../services/apiHackathon';
import { useParams } from 'react-router-dom';
import {getEmployeeBySearch} from '../services/apiTeam';
import {
    Box,
    Center,
    useColorModeValue,
    Heading,
    Button,
    Text,
    Stack,
    Image,
  } from '@chakra-ui/react';

const Ongoing=()=>{
    const navigate = useNavigate();
    const [detail,setDetail] = useState([]);
    const [employee,setEmployee] = useState([]);
    const location = useLocation();
    const id = location.state.employeeid;
    

    const loadHackathons = async ()=>{
        setDetail(await getOngoingHackathon());
    }

    useEffect(()=>{
        loadHackathons();
    },[]);

    //if new add..else edit
    const doFetch = async(hackathonid)  => {
        let a=await getEmployeeBySearch(hackathonid,id);
        var hackathonsid= hackathonid;
        if(a.length==0){
            var employeeid = id;
            navigate('/AddTeam',{state:{employeeid,hackathonsid}});
        }
        else{
            var employeeid = id;
            let teams=a[0].teamid
            var teamsid=teams;
            navigate('/EditTeam',{state:{employeeid,teamsid}});
        }
    }

    //Change to date format
    const changeDateFormat = (d) => {
        let date = new Date(d);
        let month= date.getMonth()+1;
        let result = date.getDate() + '-' + month + '-' + date.getFullYear();
        return result;
    }

    //to navigate to vote page
    const doVote = async(hackathonid)  => {
        var employeeid = id;
        var hackathonsid = hackathonid;
        navigate('/Vote',{state:{employeeid,hackathonsid}});
    }

    if(detail.length==null){
        return( <h3> No Ongoing Hackathon</h3>)
    }
    return( 
        <Fragment>
            <section className="expire">
                <div className="center">
                    <h3>Ongoing Hackathons</h3>
                </div>
                <div  className="row">
                {
                  detail.map((detail)=>{
                    return(
                        <div className="column">
                        <div className="single-expire">
                            <Center py={12}>
                              <Box role={'group'} p={6} maxW={'330px'} w={'full'} boxShadow={'2xl'} rounded={'lg'} pos={'relative'} zIndex={1}>
                                <Box
                                  rounded={'lg'}
                                  mt={-12}
                                  pos={'relative'}
                                  height={'230px'}
                                  _after={{
                                    transition: 'all .3s ease',
                                    content: '""',
                                    w: 'full',
                                    h: 'full',
                                    pos: 'absolute',
                                    top: 5,
                                    left: 0,
                                    backgroundImage: `url(${detail.poster})`,
                                    filter: 'blur(15px)',
                                    zIndex: -1,
                                  }}
                                  _groupHover={{
                                    _after: {
                                      filter: 'blur(20px)',
                                    },
                                  }}>
                                  <Image rounded={'lg'} height={230} width={282} objectFit={'cover'} src={detail.poster} onClick={()=>doFetch(detail.hackathonid)}/>
                                </Box>
                                <Stack pt={10} align={'center'}>
                                  <Text color={'gray.500'} fontSize={'sm'} textTransform={'uppercase'}>
                                    Hackathon
                                  </Text>
                                  <Heading fontSize={'2xl'} fontFamily={'body'} fontWeight={500}>
                                    {detail.hackathonname}
                                  </Heading>
                                  <Stack direction={'row'} align={'center'}>
                                    <Image height={5} width={22} objectFit={'cover'} src={'https://drive.google.com/uc?export=view&id=1kMbZOwJsDJrd3B8COrnO0kChyMJJ2DqS'} />
                                    <Text fontWeight={700} fontSize={'l'} pr={5}>
                                      {changeDateFormat(detail.hackathondate)}
                                    </Text>
                                    <Image height={5} width={22} objectFit={'cover'} src={'https://drive.google.com/uc?export=view&id=11PLH_szHogJL6z5Pi9CahqkCka40ojTs'}/>
                                    <Text fontWeight={700} fontSize={'l'}>
                                      {detail.prize}
                                    </Text>
                                  </Stack>
                                  <Stack direction={'row'}>
                                    <Button bg={'blue.400'} rounded={'full'} color={'white'} onClick={()=>doVote(detail.hackathonid)}  width={'90px'}  _hover={{ bg: 'blue.600' }}> Vote </Button>
                                    <Text pl={5}></Text>
                                  </Stack>
                                </Stack>
                              </Box>
                            </Center>
                        </div>
                    </div>
                    )
                  })  
                }  
                </div>
            </section>
        </Fragment>
    )
 }
 export default Ongoing;
