import React, { useState ,useEffect } from 'react';
import { useNavigate, useLocation } from "react-router-dom";
import { useParams } from 'react-router';
import Navbar from '../component/Navbar';
import Hero from '../component/Hero';
import Infocard from '../component/Infocard';
import "../styles/HackathonStyles.css"
import {getHackathonById } from '../services/apiHackathon';
import {
    Box,
    chakra,
    Stack,
    Text,
    Center,
    Image,
    Button,
    Flex,
    SimpleGrid,
    Stat,
    StatLabel,
    StatNumber,
    useColorModeValue,
  } from '@chakra-ui/react';

export default function Hackathon(){
    const navigate = useNavigate();
    const { id } = useParams();
    const [items,setItems] = useState([]);
    const location = useLocation();
    const employeeid = location.state.employeeid;
    const hackathonid = location.state.hackathonid;

    const  getHackathon = async (hackathonid) =>{
        setItems(await getHackathonById(hackathonid));   
    }

    useEffect(()=>{
        getHackathon(hackathonid);
    },[]);

    const BackToHackathon=()=>
    {
        navigate('/Admin',{state:{employeeid:"a123001"}});
    }

    //To change date format
    const changeDateFormat = (d) => {
        let date = new Date(d);
        let month = date.getMonth()+1;
        let result = date.getDate() + '-' + month + '-' + date.getFullYear();
        return result;
    }

    return(
        <div>
            <Navbar/>
            <Hero
                cName="hero-mid"
                heroImg="https://drive.google.com/uc?export=view&id=1TLmqZ7WuhCDZRsX2gkWa0BcIZTWHY-FG"
            />
            <Center>
                <Stack direction={'row'} align={'center'}> 
                    <Stack>
                        <Text fontWeight={900} fontSize={65}>{items.hackathonname}</Text>
                    </Stack>
                    <Stack pl={300}>
                        <Button bg={'#000'} rounded={'full'} color={'white'} onClick={()=>BackToHackathon()}>Back to hackathon</Button>
                    </Stack>
                </Stack>
            </Center>
            <Box maxW="7xl" mx={'auto'} pt={5} px={{ base: 2, sm: 12, md: 17 }}>
                <SimpleGrid columns={{ base: 1, md: 3 }} spacing={{ base: 5, lg: 8 }}>
                    <Infocard title={'Date'} stat={changeDateFormat(items.hackathondate)}/>
                    <Infocard title={'Venue'} stat={items.venue}/>
                    <Infocard title={'Prize'} stat={items.prize}/>
                </SimpleGrid>
            </Box>
            <br/>
            <div className="image1">
                <div className="hero">
                    <img alt="Winning" src="https://drive.google.com/uc?export=view&id=1_APzDEhBCwgWhpVSV72QLhlierjl0H2E"/>
                    <div className="hero-text1">
                        <Center> 
                            <Text fontWeight={300} fontSize={25} pr={'18vh'} > {items.winner}</Text>
                        </Center>
                    </div>
                </div>
                <div className="hero">
                    <img alt="Winning" src="https://drive.google.com/uc?export=view&id=1kHKOh3hDT3rrBS9xpXYt3GxOIbJzFzW2"/>
                    <div className="hero-text1">
                        <Center> 
                            <Text fontWeight={300} fontSize={25} pr={'17vh'} >{items.runner}</Text>
                        </Center>
                    </div>
                </div>
            </div>
        </div>
    )
}