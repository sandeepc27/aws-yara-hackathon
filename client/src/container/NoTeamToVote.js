import React, { useState ,useEffect } from 'react';
import '../styles/NoDataStyles.css';
import { useNavigate ,useLocation} from "react-router-dom";
import { useParams } from 'react-router';
import {
  Button,
} from '@chakra-ui/react';

export default function NoTeamToVote() {
  const {id} = useParams();
  const navigate = useNavigate();
  const location = useLocation();
  
  const backtoEmployee = ()=>{
    var employeeid=location.state.employeeid;
    navigate('/Employee',{state:{employeeid}});
  }

    return (
        <div classname='body'>
             <center><h1>Sorry.. No team has registered!!!</h1></center>
            <center><Button  bg="#0077b6" color="white" onClick={()=>backtoEmployee()}>Back To Hackathon</Button></center>
        </div>
    );
  };