import React, { useState,useEffect } from 'react';
import { useNavigate ,useLocation} from "react-router-dom";
import EmployeeNavbar from '../component/EmployeeNavbar';
import Hero from '../component/Hero';
import EmployeeTab from '../component/EmployeeTab';

export default function Employee() {
  const location = useLocation();
        return (
            <div>
              <EmployeeNavbar/>
              <Hero
                  cName="hero-mid"
                  heroImg="https://drive.google.com/uc?export=view&id=1kieEpYSpesSFkeF7ebH4-GiGBIB_uFXD"
                  />
                  <EmployeeTab/>
            </div>
          );
        
 
}
 