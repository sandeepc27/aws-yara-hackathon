import React, { useState ,useEffect } from 'react';
import {
    Container,
    Flex,
    Box,
    Heading,
    Text,
    IconButton,
    Button,
    VStack,
    HStack,
    Wrap,
    WrapItem,
    FormControl,
    FormLabel,
    Input,
    InputGroup,
    InputLeftElement,
    Textarea,
    Center,
  } from '@chakra-ui/react';
import Navbar from '../component/Navbar'
import { useNavigate,useLocation } from "react-router-dom";
import { useParams } from 'react-router';
import  {updateHackathon,getHackathonById} from "../services/apiHackathon"

  
  export default function EditHackathon() {
    const navigate = useNavigate();
    const  hackId  = useParams();
  //const [items,setItems] = useState([]);
    const [hackathon,setHackathon]= useState({hackathonid:'',hackathonname:'',poster:'',hackathondate:'',venue:'',prize:'',winner:'',runner:''});
    const location = useLocation();
    var hackathonsid = location.state.hackathonsid;
    var employeeid = location.state.employeeid;

    const handleChange = (e) => {
      setHackathon({...hackathon,[e.target.placeholder.toLowerCase()]: e.target.value });
    }

  const updateEditHackathon = async (hackathonsid) =>{
    let hackathon = await getHackathonById(hackathonsid);
    setHackathon(hackathon); 
}

useEffect(()=>{ 
      updateEditHackathon(hackathonsid);
  },[]);

  const handleSubmit = async (e) => {
    const newItem = {...hackathon};
    updateHackathon(newItem);
    doCancel();
  }
  const doCancel = ()=>{
    navigate('/Admin',{state:{employeeid}});
  }

  const changeDateFormat = (d) => {
    let date = new Date(d);
    let month=date.getMonth()+1;
    if(month<10){
        month='0'+month;
    }
    let result = date.getFullYear() + '-' + month + '-' + date.getDate();
    return result;
}
if(!hackathon){
    return( <h3>No Hackathon is registered yet</h3>)
}
    return (
        <div>
        
      <Container bg="#9DC4FB" bgImage={'https://drive.google.com/uc?export=view&id=1xw6A4HR_zSEqHspNzadnxgIwnJxPuEqe'}  bgRepeat='no-repeat' maxW="full" objectFit='cover'  mt={0} centerContent overflow="hidden" height={'930px'}>
        <Flex>
          <Box
            bg="#0077b6"
            color="white"
            borderRadius="lg"
            m={{ sm: 4, md: 16, lg: 10 }}
            p={{ sm: 5, md: 5, lg: 16 }}>
            <Box p={4}>
               <Wrap spacing={{ base: 20, sm: 3, md: 5, lg: 20 }}>
                <WrapItem>
                  <Box bg="white" borderRadius="lg" width={500}>
                  <Text fontWeight={500} fontSize={50} color="#0077b6" pl={'15%'}>Edit Hackathon</Text>
                    <Box m={8} color="#0B0E3F">
                      <VStack spacing={4}>
                        <FormControl id="name">
                          <FormLabel>Hackathon Name</FormLabel>
                          <InputGroup borderColor="#E0E1E7">
                            <Input type="text" size="md" placeholder='HackathonName' onChange={handleChange} value={hackathon.hackathonname}/>
                          </InputGroup>
                        </FormControl>
                        <FormControl id="name">
                          <FormLabel>Poster URL</FormLabel>
                          <InputGroup borderColor="#E0E1E7">
                            <Input type="text" size="md" placeholder='Poster' onChange={handleChange} value={hackathon.poster}/>
                          </InputGroup>
                        </FormControl>
                        <FormControl id="name">
                          <FormLabel>Date</FormLabel>
                          <InputGroup borderColor="#E0E1E7">
                            <Input type="date" size="md"  placeholder='HackathonDate' onChange={handleChange} value={changeDateFormat(hackathon.hackathondate)} />
                          </InputGroup>
                        </FormControl>
                        <FormControl id="name">
                          <FormLabel>Venue</FormLabel>
                          <InputGroup borderColor="#E0E1E7">
                            <Input type="text" size="md" placeholder='Venue' onChange={handleChange} value={hackathon.venue}/>
                          </InputGroup>
                        </FormControl>
                        <FormControl id="name">
                          <FormLabel>Prize</FormLabel>
                          <InputGroup borderColor="#E0E1E7">
                            <Input type="text" size="md" placeholder='Prize' onChange={handleChange} value={hackathon.prize}/>
                          </InputGroup>
                        </FormControl>
                       <HStack> <FormControl id="name" float="right">
                          <Button
                            variant="solid"
                            bg="#0077b6"
                            color="white"
                            onClick={handleSubmit}
                            _hover={{}}>
                           Update
                          </Button>
                        </FormControl>
                        <FormControl id="name" float="right">
                          <Button
                            variant="solid"
                            bg="#0077b6"
                            color="white"
                            onClick={doCancel}
                            _hover={{}}>
                           cancel
                          </Button>
                        </FormControl></HStack>
                      </VStack>
                    </Box>
                  </Box>
                </WrapItem>
              </Wrap>
            </Box>
          </Box>
        </Flex>
      </Container>
      </div>
    );
  }
