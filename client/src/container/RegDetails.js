//import "../styles/RegDetailStyles.css";
import { useNavigate ,useLocation} from "react-router-dom";
import React,{Fragment,useState,useEffect} from 'react';
import Hero from "../component/Hero";
import Navbar from "../component/Navbar";
import { useParams } from 'react-router';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Ongoing from "../component/Ongoing";
import {getTeam, deleteTeam,getTeamById,getTeamByHackthon} from "../services/apiTeam";
import TeamVotes from '../component/TeamVotes';
import WinningTeam from '../component/WinningTeam';
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
    Button,
    Text
  } from '@chakra-ui/react'


const RegDetails=()=>{
    const navigate = useNavigate();
    const [details,setDetails] = useState([]);
    const hackathonid = useParams();
    const location = useLocation();
    var hackathonsid = location.state.hackathonsid;

    const loadDetails = async ()=>{
        setDetails(await getTeamByHackthon(hackathonsid));
    }

    useEffect(()=>{
        loadDetails();
    },[]);

    //To check whether data is present
    const docheck=()=>
    {
        if(details.length==null)
            navigate('/NoTeamRegistered',{state:{employeeid:"a123001"}});
    }
    
    const doDelete = async (id) => {
        await deleteTeam(id);
        setDetails(await getTeamByHackthon(hackathonsid));
    }

    const backToHackathon=()=>
    {
            navigate('/Admin',{state:{employeeid:"a123001"}});
    }
    
    return(
        <div>
            <Hero
                cName="hero-mid"
                heroImg="https://drive.google.com/uc?export=view&id=1kieEpYSpesSFkeF7ebH4-GiGBIB_uFXD"
            />
            <Navbar/>
            <div className="backButton"><Button  onClick={()=>backToHackathon()}>Back to hackathon</Button></div>
            <Tabs>
                <div className="tab">
                    <TabList>
                        <Tab><span>Team Details</span></Tab>
                        <Tab><span>Team Votes</span></Tab>
                        <Tab><span>Winning Team</span></Tab>
                    </TabList>
                </div>
                <TabPanel>
                    <div>
                        <TableContainer>
                            <Table size='lg' variant='simple' className="tbl1">
                                <Thead> 
                                    <Tr>
                                        <Th>Team Id</Th>
                                        <Th>Team Name</Th>
                                        <Th>Problem Statement</Th>
                                        <Th>Member Id</Th>
                                        <Th>Member Name</Th>
                                        <Th>Delete</Th>
                                    </Tr>
                                </Thead>
                                <Tbody>
                                    {docheck()}
                                        {details.map((item) => {
                                            return(
                                                <Tr key={item.teamid}>
                                                    <Td>{item.teamid}</Td>
                                                    <Td>{item.teamname}</Td>
                                                    <Td>{item.problem}</Td>
                                                    <Td>
                                                        <Table><Tr></Tr>
                                                        {item.members.map(member => (
                                                            <Tr key={member.id}>
                                                                <Td><center>{member.id}</center></Td>
                                                            </Tr>
                                                        ))}
                                                        </Table>
                                                    </Td>
                                                    <Td>
                                                        <Table><Tr></Tr>
                                                            {item.members.map(member => (
                                                                <Tr key={member.id}>
                                                                    <Td><center>{member.name}</center></Td>
                                                                </Tr>
                                                            ))}
                                                        </Table>
                                                    </Td>
                                                    <Td><Button  onClick={()=>doDelete(item.teamid)}>Delete</Button></Td>
                                                </Tr>
                                            )
                                        }
                                    )}
                                </Tbody> 
                            </Table>  
                        </TableContainer>      
                    </div>
                </TabPanel>
                <TabPanel>
                    <div>      
                        <TeamVotes hackathonsid={hackathonsid}/>
                    </div> 
                </TabPanel>
                <TabPanel>
                    <div>      
                        <WinningTeam hackathonsid={hackathonsid}/>
                    </div> 
                </TabPanel>
            </Tabs>
        </div>
    )
}
export default RegDetails

