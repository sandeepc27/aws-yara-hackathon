import React, { useState ,useEffect } from 'react';
//import '../styles/AddTeamStyles.css';
import EmployeeNavbar from '../component/EmployeeNavbar'
import { useNavigate ,useLocation} from "react-router-dom";
import { useParams } from 'react-router';
import  {addTeam} from "../services/apiTeam"
import {
  Container,
  Flex,
  Box,
  Heading,
  Text,
  IconButton,
  Button,
  VStack,
  HStack,
  Wrap,
  WrapItem,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputLeftElement,
  Textarea,
  Center,
} from '@chakra-ui/react';

export default function AddTeam() {
  const navigate = useNavigate();
  const  hackId  = useParams();
  const [formValues, setFormValues] = useState([{ id: "", name: ""}])
  const [team,setTeam]= useState({teamid:'',teamname:'',members:'',problem:'',hackathonid:'',created:'',updated:''});
  const location = useLocation();
  var hackathonsid=location.state.hackathonsid;

  const handleChange = (e) => {
    setTeam({...team,[e.target.placeholder.toLowerCase()]: e.target.value });
  }

  let handleChanges = (i, e) => {
    let newFormValues = [...formValues];
    newFormValues[i][e.target.name] = e.target.value;
    setFormValues(newFormValues);
  }

  let addFormFields = () => {
    setFormValues([...formValues, {  id: "",name: "" }])
  }

  let removeFormFields = (i) => {
    let newFormValues = [...formValues];
    newFormValues.splice(i, 1);
    setFormValues(newFormValues)
  }

  useEffect(()=>{
  },[]);

  const handleSubmit = async (e) => {
    setTeam(team.hackathonid=hackathonsid)
    setTeam(team.members=formValues)
    const newItem = {...team};
    addTeam(newItem);
    var employeeid=location.state.employeeid;
    navigate('/Employee',{state:{employeeid}});
    doCancel();
  }

  const doCancel = ()=>{
    var employeeid=location.state.employeeid;
    navigate('/Employee',{state:{employeeid}});
  }
  
  return (
    <div>
      <EmployeeNavbar/>
      <div>
        <Container bg="#1f848f" bgImage={'https://drive.google.com/uc?export=view&id=1xw6A4HR_zSEqHspNzadnxgIwnJxPuEqe'}  bgRepeat='no-repeat' maxW="full" objectFit='cover'  mt={0} centerContent overflow="hidden" height={'1100px'}>
        <Flex>
          <Box
            bg="#0077b6"
            color="white"
            borderRadius="lg"
            m={{ sm: 4, md: 16, lg: 10 }}
            p={{ sm: 5, md: 5, lg: 16 }}>
            <Box p={4}>
               <Wrap spacing={{ base: 20, sm: 3, md: 5, lg: 20 }}>
                <WrapItem>
                  <Box bg="white" borderRadius="lg" width={500}>
                  <Text fontWeight={500} fontSize={50} color="#0077b6" pl={'15%'}>Add Team</Text>
                    <Box m={8} color="#0B0E3F">
                      <VStack spacing={4}>
                        <FormControl id="name">
                          <FormLabel>Team Name</FormLabel>
                          <InputGroup borderColor="#E0E1E7">
                            <Input type="text" size="md" placeholder='teamname' onChange={handleChange} value={team.teamname}/>
                          </InputGroup>
                        </FormControl>
                        <FormControl id="name">
                        {formValues.map((element, index) => (
                          <div className="form-inline" key={index}>
                            <FormLabel>Member {index+1}</FormLabel>
                            <Input type="text" placeholder='Member Id'  name="id" value={element.id || ""} onChange={e => handleChanges(index, e)} />
                            <Input type="text" name="name" placeholder='Member Name'  value={element.name || ""} onChange={e => handleChanges(index, e)} />
                            {
                              index ? 
                              <Button bg="#0077b6" color="white" type="button"  className="button remove" onClick={() => removeFormFields(index)}>Remove Member</Button> 
                                : null
                            }
                            <br/>
                          </div>
                        ))}
                        <div className="button-section">
                          <br/>
                          <Button bg="#0077b6" color="white" className="addmember" type="button" onClick={() => addFormFields()}>Add Members</Button>
                        </div>
                      </FormControl>
                      <FormControl id="name">
                        <FormLabel>Problem statement</FormLabel>
                        <InputGroup borderColor="#E0E1E7">
                          <Input type="text" size="md" placeholder='problem' onChange={handleChange} value={team.problem}/>
                        </InputGroup>
                      </FormControl>
                     <HStack> 
                      <FormControl id="name" float="right">
                          <Button variant="solid"  bg="#0077b6"  color="white" onClick={handleSubmit}  _hover={{}}> Add </Button>
                      </FormControl>
                      <FormControl id="name" float="right">
                        <Button  variant="solid"  bg="#0077b6" color="white" onClick={doCancel} _hover={{}}> cancel </Button>
                      </FormControl>
                      </HStack>
                    </VStack>
                    </Box>
                  </Box>
                </WrapItem>
              </Wrap>
            </Box>
          </Box>
        </Flex>
      </Container>
    </div>
  </div>
    
  );
};

