import {
    Button,
    Checkbox,
    Flex,
    FormControl,
    FormLabel,
    Heading,
    Input,
    Link,
    Center,
    Stack,
    Image,
    Alert,
    AlertIcon,
    AlertTitle,
    AlertDescription,
  } from '@chakra-ui/react';
import logo from "../assets/logo.svg";
import l from "../assets/loginimg.png";
import React, { useState,useEffect } from 'react';
import {useNavigate} from "react-router-dom";
import {getEmployeeById} from '../services/apiEmployee';
  
  export default function SplitScreen() {

    var usernameInput = React.createRef(); 
        var passwordInput = React.createRef();
        const [auth,setAuth] =useState([])
        const navigate = useNavigate();
        localStorage.setItem("AdminData","false");
    
        var doLogin = async() =>{
            var id=document.getElementById('name').value;
            var pass=document.getElementById('password').value;
            var a = await getEmployeeById(id);
    
            //Login authentication
            if(a.emppassword == pass && a.emprole == 'admin'){
                localStorage.setItem("AdminData","true");
                navigate("/Admin",{state:{employeeid}});}
            else if(a.emppassword == pass && a.emprole == 'employee'){
                var employeeid=a.empid
                navigate("/Employee",{state:{employeeid}});}
            else{
              alert("Enter correct username password");
              }      
        }
    
    return (
        <div>
      <Stack minH={'100vh'} direction={{ base: 'column', md: 'row' }}>
         <Flex flex={2}>
          <Image
            alt={'Login Image'}
            objectFit={'cover'}
            src={
             l
            }
          />
        </Flex>
        <Flex p={8} flex={1} align={'center'} justify={'center'}>
          <Stack spacing={4} w={'full'} maxW={'md'}>
          <div className='header'>
         <Center><Image
          h={'100px'}
          w={'100px'}
          src={
            logo
          }
          objectFit={'cover'}
        />    </Center>    
       <Center><Heading fontSize={'3xl'} pl={'20px'}>Yara Yearly Hackathon </Heading> </Center>
           </div>
            <FormControl id="name" ref={usernameInput}>
              <FormLabel>Employee Id:</FormLabel>
              <Input type="text" />
            </FormControl>
            <FormControl id="password" ref={passwordInput}>
              <FormLabel>Password</FormLabel>
              <Input type="password" />
            </FormControl>
            <Stack spacing={6}>
              <Stack
                direction={{ base: 'column', sm: 'row' }}
                align={'start'}
                justify={'space-between'}>
                <Checkbox>Remember me</Checkbox>
                <Link color={'blue.500'}>Forgot password?</Link>
              </Stack>
              <Button colorScheme={'blue'} variant={'solid'} onClick={doLogin}>
                Sign in
              </Button>
            </Stack>
          </Stack>
        </Flex>
      </Stack>
      </div>
    );
  }