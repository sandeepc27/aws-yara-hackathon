import React, { useState ,useEffect } from 'react';
import '../styles/NoDataStyles.css';
import Navbar from '../component/Navbar'
import { useNavigate,useLocation } from "react-router-dom";
import { useParams } from 'react-router';
import  {addHackathon} from "../services/apiHackathon"
import {
  Button,
} from '@chakra-ui/react';

export default function NoTeamVote() {
  const navigate = useNavigate();
  const location = useLocation();
  var employeeid = location.state.employeeid;

  const backtoAdmin = ()=>{
    navigate('/Admin',{state:{employeeid}});
  }
    return (
        <div classname='body'>
             <center><h1>Sorry.. No One has voted yet!!!</h1></center>
            <center><Button  bg="#0077b6" color="white" onClick={()=>backtoAdmin()}>Back To Admin</Button></center>
        </div>
    );
  };