import "../styles/RegDetailStyles.css";
import { useNavigate,useLocation } from "react-router-dom";
import React,{Fragment,useState,useEffect} from 'react';
import Hero from "../component/Hero";
import { useParams } from 'react-router';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import {addVote,getVoteBySearch} from "../services/apiVote";
import {getTeamByHackthon} from "../services/apiTeam";
import EmployeeNavbar from "../component/EmployeeNavbar";
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
    Button,
    Text,
    Center
  } from '@chakra-ui/react'

const EmployeeVote=()=>{
    const navigate = useNavigate();
    const [details,setDetails] = useState([]);
    const hackathonid = useParams();
    const location = useLocation();
    var employeeid =location.state.employeeid;
    var hackathonsid =location.state.hackathonsid;

    const loadDetails = async ()=>{
        setDetails(await getTeamByHackthon(hackathonsid));
    }
    
    useEffect(()=>{
        loadDetails();
    },[]);

    //if new vote else display already voted
    const doVote = async(id)  => {
        let a = await getVoteBySearch(employeeid);
        if(a.length == null){
            await addVote(employeeid,id);
            navigate('/AlreadyVoted',{state:{employeeid}});
        }
        else{
            navigate('/AlreadyVoted',{state:{employeeid}});
        }
    }

    //To check whether data is present
    const docheck=()=>
    {
        if(details.length==null)
            navigate('/NoTeamToVote',{state:{employeeid}});
    }

    return(
        <div>
            <Hero
                cName="hero-mid"
                heroImg="https://drive.google.com/uc?export=view&id=1TLmqZ7WuhCDZRsX2gkWa0BcIZTWHY-FG"
            />
            <EmployeeNavbar/>
            <div>
                <Center><Text fontWeight={700} fontSize={45}>Vote Section</Text></Center>
                <Table className="tbl1">
                <Thead> 
                    <Tr>
                        <Th>Team Id</Th>
                        <Th>Team Name</Th>
                        <Th>Problem Statement</Th>
                        <Th>Member Id</Th>
                        <Th>Member Name</Th>
                        <Th>Vote</Th>
                    </Tr>
                </Thead>
                <Tbody>
                {docheck()}
                    {details.map((item) => {
                        return(
                            <Tr key={item.teamid}>
                                <Td>{item.teamid}</Td>
                                <Td>{item.teamname}</Td>
                                <Td>{item.problem}</Td>
                                <Td>
                                    <Table><Tr></Tr>
                                        {item.members.map(member => (
                                            <Tr key={member.id}>
                                                <Td><center>{member.id}</center></Td>
                                            </Tr>
                                        ))}
                                    </Table>
                                </Td>
                                <Td>
                                    <Table className="tbl1"><tr></tr>
                                        {item.members.map(member => (
                                            <Tr key={member.id}>
                                            <Td><center>{member.name}</center></Td>
                                            </Tr>
                                        ))}
                                    </Table>
                                </Td>
                                <Td><Button onClick={()=>doVote(item.teamid)} bgColor={'#000'} color={'#fff'}> Vote</Button></Td>
                            </Tr>
                        )
                    }
                )}
            </Tbody> 
        </Table>        
    </div>
</div>
)
}
export default EmployeeVote

