import React, { useState ,useEffect } from 'react';
import '../styles/AlreadyVotedStyles.css';
import EmployeeNavbar from '../component/EmployeeNavbar'
import { useNavigate,useLocation } from "react-router-dom";
import { useParams } from 'react-router';
import {
  Box,
  Center,
  useColorModeValue,
  Heading,
  Button,
  Text,
  Stack,
  Image,
} from '@chakra-ui/react';

export default function AlreadyVoted() {
  const navigate = useNavigate();
  const {id} = useParams();
  const location = useLocation();
  var employeeid=location.state.employeeid

  const backtoHackathon = ()=>{
    navigate('/Employee',{state:{employeeid}});
  }
    return (
      <div>
        <EmployeeNavbar/>
          <div>
            <div className="hero">
              <Image rounded={'lg'}  height={'100%'}  width={'100%'}  objectFit={'cover'}  src={'https://drive.google.com/uc?export=view&id=1WRYkuL9gZPPLjYfo7eLYuj3nKq1bqQN6'} />                  <div className="hero-text">
                <center>
                  <form className="form1">
                    <h2 className="voteText">You Have Voted!!</h2>
                    <Button bg={'#000'} rounded={'full'} color={'white'}  onClick={()=>backtoHackathon()} _hover={{ bg: '#7c7c7c' }}> Back To Hackathon </Button>
                  </form>
                </center>
              </div>
            </div>
          </div>
        </div>
    );
  };