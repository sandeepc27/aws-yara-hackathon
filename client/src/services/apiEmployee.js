// import axios from "axios";
//  const baseURL = process.env.NODE_ENV === 'production'?'api/employee':'http://localhost:2000/api/employee'
// export default axios.create({
//   baseURL,
// })
const apiEndPoint=process.env.NODE_ENV === 'production'?'api/employee':'http://localhost:2000/api/employee'

    export const getEmployeeById = async (id) =>{
        return fetch(apiEndPoint + "/"+id, {
          method: 'GET',
              headers: {'Content-Type': 'application/json;charset=utf-8'}
          })
        .then(response => response.json())
        .then(response => { return response; })
        .catch((error) => { console.log(error); });
      }