//const apiEndPoint = 'http://localhost:2000/api/hackathon'

const apiEndPoint=process.env.NODE_ENV === 'production'?'api/hackathon':'http://localhost:2000/api/hackathon'


export const getHackathon = async () => {
    return fetch(apiEndPoint, {
     method: 'GET', 
      headers: {'Content-Type': 'application/json'},
      })
      .then((response) => response.json())
      .then((data) => {return data;})
      .catch((error) => { console.error('Error:', error); });
  };
  export const getExpiredHackathon = async () => {
    return fetch(apiEndPoint+"/expiredHackathon", {
     method: 'GET', 
      headers: {'Content-Type': 'application/json'},
      })
      .then((response) => response.json())
      .then((data) => {return data;})
  };

  export const getOngoingHackathon = async () => {
    return fetch(apiEndPoint+"/ongoingHackathon", {
     method: 'GET', 
      headers: {'Content-Type': 'application/json'},
      })
      .then((response) => response.json())
      .then((data) => {return data;})
      .catch((error) => { console.error('Error:', error); });
  };
  export const addHackathon = async (record) =>{
    return fetch(apiEndPoint, {
      method: 'POST',
          headers: {'Content-Type': 'application/json;charset=utf-8'},
          body:JSON.stringify(record)
      })
    .then(response => response.json())
    .then(response => { return response;})
    .catch((error) => { console.log(error); });
  }

export const getHackathonById = async (id) =>{
    return fetch(apiEndPoint+"/"+id, {
      method: 'GET',
          headers: {
	    'Accept': 'application/json',
    'Content-Type': 'application/json'}
      })
    .then(response => response.json())
    .then(response => { console.log(response); return response })
    .catch((error) => { console.log(error); });
  }

  export const updateHackathon = async (record) =>{
    return fetch(apiEndPoint, {
      method: 'PUT',
          headers: {'Content-Type': 'application/json;charset=utf-8'},
          body:JSON.stringify(record)
      })
    .then(response => response.json())
    .then(response => {  return response; })
    .catch((error) => { console.log(error); });
  } 

  export const updateWinners = async (record) =>{
    return fetch(apiEndPoint+"/winners", {
      method: 'PUT',
          headers: {'Content-Type': 'application/json;charset=utf-8'},
          body:JSON.stringify(record)
      })
    .then(response => response.json())
    .then(response => {  return response; })
    .catch((error) => { console.log(error); });
  } 