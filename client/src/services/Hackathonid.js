import axios from "axios";
const baseURL= process.env.NODE_ENV === 'production'?'api/hackathon':'http://localhost:2000/api/hackathon'
export default axios.create({
  baseURL,
 })