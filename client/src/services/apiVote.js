//const apiEndPoint = 'http://localhost:2000/api/vote'
const apiEndPoint=process.env.NODE_ENV === 'production'?'api/vote':'http://localhost:2000/api/vote'

export const getVotes = async () => {
    return fetch(apiEndPoint, {
     method: 'GET', 
      headers: {'Content-Type': 'application/json'},
      })
      .then((response) => response.json())
      .then((data) => {return data;})
      .catch((error) => { console.error('Error:', error); });
  };

  export const getVoteBySearch = async (id) =>{
    return fetch(apiEndPoint + "/"+id, {
      method: 'GET',
          headers: {'Content-Type': 'application/json;charset=utf-8'}
      })
    .then(response => response.json())
    .then(response => { 
      return response; })
    .catch((error) => { console.log(error); });
  }

  export const addVote = async ( empid , teamid ) =>{
    return fetch(apiEndPoint+"/"+empid+"/"+teamid, {
      method: 'POST',
          headers: {'Content-Type': 'application/json;charset=utf-8'},
      })
    .then(response => response.json()) 
    .then(response => {
      return response;})
    .catch((error) => { console.log(error); });
  }

  export const getVotesCount = async () => {
    return fetch(apiEndPoint, {
     method: 'GET', 
      headers: {'Content-Type': 'application/json'},
      })
      .then((response) => response.json())
      .then((data) => {return data;})
      .catch((error) => { console.error('Error:', error); });
  };

  export const doTruncate = async () => {
    return fetch(apiEndPoint, {
     method: 'DELETE', 
      headers: {'Content-Type': 'application/json'},
      })
      .then((response) => response.json())
      .then((data) => {return data;})
      .catch((error) => { console.error('Error:', error); });
  };

// const apiEndPoint = 'http://localhost:4000/api/vote'

// //const {getVotes , addVote , getVotesCount , doTruncate} = require('../service/Database')

// export const getVotes = async () => {
//     return fetch(apiEndPoint, {
//      method: 'GET', 
//       headers: {'Content-Type': 'application/json'},
//       })
//       .then((response) => response.json())
//       .then((data) => {return data;})
//       .catch((error) => { console.error('Error:', error); });
//   };

//   export const addVote = async (record) =>{
//     return fetch(apiEndPoint, {
//       method: 'POST',
//           headers: {'Content-Type': 'application/json;charset=utf-8'},
//           body:JSON.stringify(record)
//       })
//     .then(response => response.json())
//     .then(response => { return response;})
//     .catch((error) => { console.log(error); });
//   }

//   export const getVotesCount = async () => {
//     return fetch(apiEndPoint, {
//      method: 'GET', 
//       headers: {'Content-Type': 'application/json'},
//       })
//       .then((response) => response.json())
//       .then((data) => {return data;})
//       .catch((error) => { console.error('Error:', error); });
//   };

//   export const doTruncate = async () => {
//     return fetch(apiEndPoint, {
//      method: 'DELETE', 
//       headers: {'Content-Type': 'application/json'},
//       })
//       .then((response) => response.json())
//       .then((data) => {return data;})
//       .catch((error) => { console.error('Error:', error); });
//   };
