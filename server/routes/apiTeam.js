var express = require('express');
var router = express.Router();

const { getTeam , getTeamById , addTeam , updateTeam , getEmployeeBySearch , deleteTeam , getTeamByHackthon } = require('../service/Database')

router.get('/', async ( req , res ) => {
    const records = await getTeam();
    res.send(records);
});

router.post('/', async ( req , res ) => {
    let team = req.body;
    console.log(team)
    await addTeam(team)
    res.send({ result:'success' , msg:'recorded added'});
});

router.put('/' , async ( req , res) => {
    let team = req.body;
    console.log(team);
    await updateTeam(team);
    res.send({ result:'success' , msg:'record updated'});
});

router.get('/:id', async ( req , res ) => {
    const id = req.params.id;
    const team = await getTeamById(id);
    res.send(team);
});

router.get('/hackathon/:id', async ( req , res ) => {
    const id = req.params.id;
    const team = await getTeamByHackthon(id);
    res.send(team);
});

router.get('/search/:field/:searchWord', async function (req, res) {
    const hackathonid = req.params.field;
	const searchWord = req.params.searchWord;
	const list = await getEmployeeBySearch(hackathonid,searchWord);
    console.log(list);
    res.send(list);
});

router.delete('/:id', async ( req , res ) => {
    const id = req.params.id;
    console.log(id)
    const team = await deleteTeam(id);
    res.send(team);
});

module.exports = router;
