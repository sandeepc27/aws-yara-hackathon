var express = require('express');
var router = express.Router();

const {getVotes , getVoteBySearch , addVote , getVotesCount , doTruncate} = require('../service/Database')

router.get('/', async ( req , res ) => {
    const count = await getVotesCount();
    res.send(count);
});

router.get('/:id', async ( req , res ) => {
    const id = req.params.id;
    const value = await getVoteBySearch(id);
    res.send(value);
    
});

router.post('/:empid/:teamid', async function (req, res) {
    const empid = req.params.empid;
	const teamid = req.params.teamid;
	const value = await addVote(empid,teamid);
    res.send(value);
});

router.delete('/', async ( req , res ) => {
    const count = await doTruncate();
    res.send({ result:'success' , msg:'records reset done'});
});

module.exports = router;
