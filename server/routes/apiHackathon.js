var express = require('express');
var router = express.Router();

const {getHackathon , getHackathonById , getExpiredHackathon , getOngoingHackathon , addHackathon , updateHackathon, updateWinners } = require('../service/Database')

router.get('/', async ( req , res ) => {
    const records = await getHackathon();
    res.send(records);
});

router.get('/expiredHackathon', async ( req , res ) => {
    const records = await getExpiredHackathon();
    res.send(records);
});

router.get('/ongoingHackathon', async ( req , res ) => {
    const records = await getOngoingHackathon();
    res.send(records);
});

router.post('/', async ( req , res ) => {
    let hackathon = req.body;
    await addHackathon(hackathon)
    res.send({ result:'success' , msg:'recorded added'});
});

router.put('/' , async ( req , res) => {
    let hackathon = req.body;
    await updateHackathon(hackathon);
    res.send({ result:'success' , msg:'record updated'});
});

router.put('/winners' , async ( req , res) => {
    let hackathon = req.body;
    await updateWinners(hackathon);
    res.send({ result:'success' , msg:'record updated'});
});

router.get('/:id', async ( req , res ) => {
    const id = req.params.id;
    const hackathon = await getHackathonById(id);
    res.send(hackathon);
});

module.exports = router;
