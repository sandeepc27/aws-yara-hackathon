var express = require('express');
var router = express.Router();

// /users
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

// /users/login
router.post('/login', function(req, res, next) {
  console.log(" received: ",req.body);
  // req request from browser
  // res resonse to browser
  if(req.body.username == req.body.password && typeof(req.body.username) != 'undefined' && req.body.username.length > 0){
  	req.session.user = req.body.username;
    res.send({result:'success', msg:"user login successful."});
  }else{
  	res.send({result:'fail', msg:"user login failed."});
  }
});

module.exports = router;
